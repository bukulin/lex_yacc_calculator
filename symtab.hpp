#ifndef SYMTAB_HPP
#define SYMTAB_HPP

struct symtab
{
	char* name;
	double (*function)(double);
	double value;
};

symtab* symlook(const char* name);
void addfunc(const char* name, double(*function)(double));
void cleanup();

#endif
