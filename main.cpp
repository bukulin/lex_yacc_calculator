#include "calculatorParser.h"
#include "calculatorScanner.h"
#include "symtab.hpp"

#include <iostream>

#include <cmath>

extern FILE* yyin;

void yyerror(yyscan_t scanner, double& result, const char* s)
{
	std::cerr << s << std::endl;
}

int main()
{
	addfunc("sqrt", sqrt);
	addfunc("exp", exp);
	addfunc("log", log);

	yyscan_t scanner;
	yylex_init(&scanner);
	yyset_in(stdin, scanner);

	do {
		double result;
		auto res = yyparse(scanner, result);
		std::cout << "(" << res << ")=> " << result << std::endl;
	} while(!feof(yyget_in(scanner)));

	yylex_destroy(scanner);

	cleanup();

	return 0;
}
