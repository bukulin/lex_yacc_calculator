#include "symtab.hpp"

#include <array>
#include <algorithm>
#include <cstring>

static std::array<symtab, 20> symbolTable;

symtab* symlook(const char* name)
{
	const auto found =
		std::find_if(symbolTable.begin(), symbolTable.end(),
		     [name](const auto each){
			     return each.name && strcmp(each.name, name) == 0;
		     });
	if (found != symbolTable.end())
		return found;

	const auto firstEmpty =
		std::find_if(symbolTable.begin(), symbolTable.end(),
			     [](const auto each){
				     return each.name == nullptr;
			     });
	if (firstEmpty == symbolTable.end())
		throw std::runtime_error("Too many symbols defined!");

	firstEmpty->name = strdup(name);

	return firstEmpty;
}

void addfunc(const char* name, double(*function)(double))
{
	auto sp = symlook(name);
	sp->function = function;
}

void cleanup()
{
	for(auto& each: symbolTable) {
		if (each.name == nullptr)
			continue;
		free(each.name);
		each.name = nullptr;
	}
}
