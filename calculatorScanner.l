%option noyywrap
%option warn
%option ecs
%option stack
%option pointer
%option reentrant
%option bison-bridge

%{
#include "symtab.hpp"
#include "calculatorParser.h"
#include <stdlib.h>
%}

%%
([0-9]+|([0-9]*\.[0-9]+)([eE][-+]?[0-9]+)?)	{ yylval->doubleValue = strtod(yytext, nullptr); return NUMBER; }

[ \t]		;		/* ignore whitespace */

[A-Za-z][A-Za-z0-9]*		{ yylval->symbol = symlook(yytext); return NAME; }

"$"		return 0;	/* end of input */

\n|.		return yytext[0];

%%
