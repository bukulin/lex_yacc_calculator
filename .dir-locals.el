;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode
  (cmake-ide-build-dir . "~/work/snippets/lex_yacc_calculator/build/"))
 (c-mode
  (cmake-ide-build-dir . "~/work/snippets/lex_yacc_calculator/build/"))
 (bison-mode
  (cmake-ide-build-dir . "~/work/snippets/lex_yacc_calculator/build/")))
