%define api.pure full
%lex-param{void* scanner}
%parse-param{void* scanner}
%parse-param{double& result}

%{
#include "symtab.hpp"
#include "calculatorParser.h"
#include "calculatorScanner.h"
#include <iostream>

void yyerror(yyscan_t scanner, double& result, const char*);
double variableTable[26];
%}

%union {
	double doubleValue;
	struct symtab* symbol;
}

%token	<symbol> NAME
%token	<doubleValue> NUMBER

%left '+' '-'
%left '*' '/'
%precedence UMINUS

%type	<doubleValue>	expression

%%

statement_list:	 statement '\n'
	|	statement_list statement '\n'
	;

statement:	NAME '=' expression	{ $1->value = $3; }
	|	expression		{ result = $1; }
	;

expression:	expression '+' expression	{ $$ = $1 + $3; }
	|	expression '-' expression	{ $$ = $1 - $3; }
	|	expression '*' expression	{ $$ = $1 * $3; }
	|	expression '/' expression	{ if ($3 == 0.0) yyerror(scanner, result, "divide by zero"); else $$ = $1 / $3; }
	|	'-' expression %prec UMINUS	{ $$ = -$2; }
	|	'(' expression ')'		{ $$ = $2; }
	|	NUMBER				{ $$ = $1; }
	|	NAME				{ $$ = $1->value; }
	|	NAME '(' expression ')'	{ if ($1->function) { $$ = $1->function($3); } else { printf("%s not a function\n", $1->name); $$ = 0.0; } }
	;

%%
